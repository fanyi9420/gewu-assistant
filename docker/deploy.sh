#!/bin/sh

usage(){
	echo "Usage: sh 执行脚本.sh [port|base|modules|stop|rm]"
	exit 1
}

# 开启所需端口
port(){
	firewall-cmd --add-port=9420/tcp --permanent
	firewall-cmd --add-port=9421/tcp --permanent
	firewall-cmd --add-port=8848/tcp --permanent
	firewall-cmd --add-port=9418/tcp --permanent
	firewall-cmd --add-port=9428/tcp --permanent
	firewall-cmd --add-port=9429/tcp --permanent
	firewall-cmd --add-port=9422/tcp --permanent
	firewall-cmd --add-port=9423/tcp --permanent
	firewall-cmd --add-port=9424/tcp --permanent
	firewall-cmd --add-port=9425/tcp --permanent
	firewall-cmd --add-port=9426/tcp --permanent
	firewall-cmd --add-port=9427/tcp --permanent
	service firewalld restart
}

# 启动基础环境（必须）
base(){
	docker-compose up -d gewu-redis gewu-nacos
}

# 启动程序模块（必须）
modules(){
	docker-compose up -d gewu-gateway gewu-auth gewu-modules-system
}

# 关闭所有环境/模块
stop(){
	docker-compose stop
}

# 删除所有环境/模块
rm(){
	docker-compose rm
}

# 根据输入参数，选择执行对应方法，不输入则执行使用说明
case "$1" in
"port")
	port
;;
"base")
	base
;;
"modules")
	modules
;;
"stop")
	stop
;;
"rm")
	rm
;;
*)
	usage
;;
esac
