#!/bin/sh

# 复制项目的文件到对应docker路径，便于一键生成镜像。
usage() {
	echo "Usage: sh copy.sh"
	exit 1
}


# copy sql
#echo "begin copy sql "
#cp ../sql/ry_20210908.sql ./mysql/db
#cp ../sql/ry_config_20211118.sql ./mysql/db

# copy html
echo "begin copy html "
cp -r ../gewu-ui/dist/** ./nginx/html/dist


# copy jar
echo "begin copy gewu-gateway "
cp ../gewu-gateway/target/gewu-gateway.jar ./gewu/gateway/jar

echo "begin copy gewu-auth "
cp ../gewu-auth/target/gewu-auth.jar ./gewu/auth/jar

echo "begin copy gewu-visual "
cp ../gewu-visual/gewu-monitor/target/gewu-visual-monitor.jar  ./gewu/visual/monitor/jar

echo "begin copy gewu-modules-system "
cp ../gewu-modules/gewu-system/target/gewu-modules-system.jar ./gewu/modules/system/jar

echo "begin copy gewu-modules-file "
cp ../gewu-modules/gewu-file/target/gewu-modules-file.jar ./gewu/modules/file/jar

echo "begin copy gewu-modules-job "
cp ../gewu-modules/gewu-job/target/gewu-modules-job.jar ./gewu/modules/job/jar

echo "begin copy gewu-modules-gen "
cp ../gewu-modules/gewu-gen/target/gewu-modules-gen.jar ./gewu/modules/gen/jar

