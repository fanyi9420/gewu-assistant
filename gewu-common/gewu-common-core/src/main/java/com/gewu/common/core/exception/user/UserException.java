package com.gewu.common.core.exception.user;

import com.gewu.common.core.exception.base.BaseException;

/**
 * 用户信息异常类
 * 
 * @author gewu
 */
public class UserException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public UserException(String code, Object[] args)
    {
        super("user", code, args, null);
    }
}
