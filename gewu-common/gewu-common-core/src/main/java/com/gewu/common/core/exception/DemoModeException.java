package com.gewu.common.core.exception;

/**
 * 演示模式异常
 * 
 * @author gewu
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
