import {
  app,
  BrowserWindow,
  ipcMain,
  Menu, powerMonitor, Tray
} from 'electron'
//引入update.js
import { updateHandle } from '../renderer/utils/Update.js';

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path')
    .join(__dirname, '/static')
    .replace(/\\/g, '\\\\')
}
let mainWindow
const winURL = process.env.NODE_ENV === 'development' ?
  `http://localhost:9420` :
  `file://${__dirname}/index.html`

//const express = require('/app.js');

const express = require("express");
const apicache = require("apicache");
const path = require("path");
const fs = require("fs");
let cache = apicache.middleware;
//隐藏主窗口，并创建托盘 引用放外部，防止被当垃圾回收
let appTray = null;
// 系统托盘右键菜单
let trayMenuTemplate = null
import routes from './routers'

function createWindow() {
  const app = express();

  // 跨域设置
  app.all("*", function (req, res, next) {
    if (req.path !== "/" && !req.path.includes(".")) {
      res.header("Access-Control-Allow-Credentials", true);
      // 这里获取 origin 请求头 而不是用 *
      res.header("Access-Control-Allow-Origin", req.headers["origin"] || "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
      res.header("Content-Type", "application/json;charset=utf-8");
    }
    next();
  });
  const onlyStatus200 = (req, res) => res.statusCode === 200;

  app.use(cache("2 minutes", onlyStatus200));

  app.use(express.static(path.resolve(__dirname, "public")));

  app.use(function (req, res, next) {
    const proxy = req.query.proxy;
    if (proxy) {
      req.headers.cookie = req.headers.cookie + `__proxy__${proxy}`;
    }
    next();
  });


  app.use('/', routes);
  const port = 3000;
  app.listen(port, () => {
    console.log(`server running @ http://localhost:${port}`);
  });

  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 800,
    minHeight: 400,
    useContentSize: true,
    width: 1200,
    minWidth: 608,
    frame: false,
    resizable: true,
    skipTaskbar: false,
    icon: __static + '/common/logo.png',
    title: "格物助手",
    autoHideMenuBar: true,
    webPreferences: {
      webviewTag: true
    }

  });

  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  //设置版本更新地址，即将打包后的latest.yml文件和exe文件同时放在    
  //http://xxxx/test/version/对应的服务器目录下,该地址和package.json的publish中的url保持一致
  let feedUrl = "https://www.yigefan.com/app/";
  //检测版本更新
  updateHandle(mainWindow, feedUrl);
}

app.on('ready', createWindow)

//防止应用隐藏后多次点击桌面图标打开多个窗口启动了多个node服务
const isSecondInstance = app.makeSingleInstance((commandLine, workingDirectory) => {
  if (mainWindow) {
    if (mainWindow.isMinimized()) mainWindow.restore()
    mainWindow.focus()
    mainWindow.show()
  }
})
if (isSecondInstance) {
  app.quit()
}

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

ipcMain.on('playMusic', (event, data) => {
  appTray.setToolTip(data);
});
//获取系统用户目录
ipcMain.on('getUserConfigData', () => {
  sendMainData({
    'getHome': app.getPath('home'), // 用于home目录
    'getUserData': app.getPath('userData'), // 获取用户根目录
    'getAppData': app.getPath('appData'), // 用于存储 app 用户数据目录
    'getDesktop': app.getPath('desktop')// 桌面目录
  })
});

//监听渲染进程获取系统配置
ipcMain.on('getSysSettingData', () => {
  mainWindow.webContents.send('backSysSettingData', {
    "powerOn": app.getLoginItemSettings().openAtLogin
  })
});

//监听渲染进程设置开机启动
ipcMain.on('setPowerOn', () => {
  powerOnSetting()
});

ipcMain.on('quitApp', () => {
  app.quit()
});
ipcMain.on('close', e => {
  mainWindow.hide()
})
ipcMain.on('minimize', e => {
  mainWindow.minimize()
})
ipcMain.on('maximize', e => {
  if (!mainWindow.isMaximized()) {
    mainWindow.maximize()
  } else {
    mainWindow.unmaximize()
  }
})
//发送路径信息
function sendMainData(data) {
  mainWindow.webContents.send('backMainData', data)
}

//发送跳转的页面信息
function pageSwitch(data) {
  mainWindow.webContents.send('pageSwitch', data)
}

/* 
创建新的窗口 
*/
let newWin
ipcMain.on('loadByNewWindow', (event, data) => {
  newWin = new BrowserWindow({
    height: 800,
    minHeight: 400,
    useContentSize: true,
    width: 1200,
    minWidth: 608,
    frame: false,
    resizable: true,
    skipTaskbar: false,
    icon: __static + '/common/logo.png',
    title: "格物助手",
    autoHideMenuBar: true,
    webPreferences: {
      webviewTag: true
    }
  })

  const view = new BrowserView()
  view.setBounds({
    width: 1300, height: 800
  })
  view.setAutoResize({ width: true, height: true })
  // BrowserView 嵌套网页
  view.webContents.loadURL(data)
  newWin.setBrowserView(view)
  // 开启窗口之后，需要定义关闭窗口指针为空，防止内存溢出
  newWin.on('close', () => {
    newWin = null
  })
})

//开机自启动设计
function powerOnSetting() {
  if (!app.isPackaged) {
    app.setLoginItemSettings({
      openAtLogin: !app.getLoginItemSettings().openAtLogin,
      path: process.execPath
    })
  } else {
    app.setLoginItemSettings({
      openAtLogin: !app.getLoginItemSettings().openAtLogin
    })
  }
}

//加载托盘
app.on('ready', setTray)
//绑定关闭事件
function setTray() {
  trayMenuTemplate = [{
    label: '显示主窗口',
    click: function () {
      mainWindow.show();;
    }
  },
  {
    label: '设置',
    click: function () {
      //打开相应页面
      pageSwitch('/setting')
    }
  },
  {     // 系统托盘图标目录
    label: '退出',
    // icon: __static + '/common/poweroff.png',
    click: function () {
      app.quit();
    }
  }
  ];
  // 当前目录下的app.ico图标
  appTray = new Tray(__static + '/common/logo.png');
  // 图标的上下文菜单
  const contextMenu = Menu.buildFromTemplate(trayMenuTemplate);
  // 设置托盘悬浮提示
  appTray.setToolTip('格物助手');
  // 设置托盘菜单
  appTray.setContextMenu(contextMenu);
  // 单击托盘小图标显示应用
  appTray.on('click', function (event) {
    // 显示主程序
    mainWindow.show();
  });

  // 右键点击托盘显示
  // appTray.on('right-click', function (event, Rectangle) {
  //   mainWindow.webContents.send('message', Rectangle)
  // });

}