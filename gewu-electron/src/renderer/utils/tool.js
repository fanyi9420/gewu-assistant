export function isURL(strUrl) {
  var regular = /^\b(((https?|ftp):\/\/)?[-a-z0-9]+(\.[-a-z0-9]+)*\.(?:com|edu|gov|int|mil|net|org|biz|info|name|museum|asia|coop|aero|[a-z][a-z]|((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]\d)|\d))\b(\/[-a-z0-9_:\@&?=+,.!\/~%\$]*)?)$/i
  if (regular.test(strUrl)) {
    return true;
  }
  else {
    return false;
  }
}


export function timeFix() {
  const time = new Date()
  const hour = time.getHours()
  return hour < 9 ? '早上好' : (hour < 12 ? '上午好' : (hour <= 13 ? '中午好' : (hour < 20 ? '下午好' : '晚上好')))
}''

/**
  * 使用test方法实现模糊查询
  * @param  {Array}  list     原数组
  * @param  {String} keyWord  查询的关键词
  * @return {Array}           查询的结果
  */
export function fuzzyQuery(list, keyWord) {
  var reg = new RegExp(keyWord);
  var arr = [];
  for (var i = 0; i < list.length; i++) {
    if (reg.test(list[i])) {
      arr.push(list[i]);
    }
  }
  return arr;
}

export function findSubStr(str1, str2) {
  var result = ''
  if (!str1 || !str2 || str2.length < 1) {
    return result
  }
  var index1 = findIndexByTime(str1, '/', 2);
  var index2 = findIndexByTime(str2, '/', 2);
  var strSub1 = str1.substring(0, index1);
  if (strSub1 == str2.substring(0, index2)) {
    result = strSub1;
  }
  return result
}

export function isHasSameUrl(str1, str2) {
  var url = findSubStr(str1, str2)
  return isURL(url)
}

export function findIndexByTime(str, cha, num) {
  var x = str.indexOf(cha);
  for (var i = 0; i < num; i++) {
    x = str.indexOf(cha, x + 1);
  }
  return x;
}