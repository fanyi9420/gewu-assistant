const iconConfig = {
  'setting': 'el-icon-setting',
  'tool': 'el-icon-s-tools',
  'user': 'el-icon-user',
  'user-info': 'el-icon-postcard',
  'logout': 'el-icon-switch-button',
  'clock': 'el-icon-alarm-clock',
}
const lowdb = window.require('lowdb')
let fileSync = window.require('lowdb/adapters/FileSync')
const os = window.require('os')
const path = window.require('path');
let adapter = new fileSync(path.join(os.homedir(), 'yf_config.json'))
const db = lowdb(adapter)

export default {
  db, iconConfig
}