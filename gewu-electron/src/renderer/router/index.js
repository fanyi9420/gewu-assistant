import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      redirect: { name: 'menu' },
      component: require('@/views/main').default,
      children: [
        {
          path: 'menu',
          name: 'menu',
          component: () => import('../views/menu/index.vue')
        },
        {
          path: 'web',
          name: 'web',
          component: () => import('../views/web/index.vue')
        },
        {
          path: 'music',
          name: 'music',
          component: () => import('../views/music/index.vue')
        }, {
          path: 'playlist-detail',
          name: 'playlist-detail',
          component: () => import('../views/playlist/detail')
        },
        {
          path: 'video',
          name: 'video',
          component: () => import('../views/video/index.vue')
        },
        {
          path: 'setting',
          name: 'setting',
          component: () => import('../views/setting/index.vue')
        },
        {
          path: 'tool',
          name: 'tool',
          component: require('@/views/tool').default,
          children: [{
            path: 'timer',
            name: 'timer',
            component: () => import('../views/tool/timer/index.vue')
          }, {
            path: 'note',
            name: 'note',
            component: () => import('../views/tool/note/index.vue')
          }, {
            path: 'calendar',
            name: 'calendar',
            component: () => import('../views/tool/calendar/index.vue')
          }, {
            path: 'mouseDraw',
            name: 'mouseDraw',
            component: () => import('../views/tool/mouseDraw/mouseDraw.vue')
          }]
        }
      ]
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
