export default {
    state: {
        isCollapse: true,
        isMaximized: false,
    },
    mutations: {
        SET_COLLAPSE: (state, data) => {
            state.isCollapse = data
        },
        SET_MAX: (state, data) => {
            state.isMaximized = data
        },
    },
    actions: {
    }
}
