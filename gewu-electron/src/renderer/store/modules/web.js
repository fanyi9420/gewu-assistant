export default {
  state: {
    webPath: '',
    webTitle: '',
    webTypes: []
  },
  mutations: {
    SET_WEB_PATH: (state, data) => {
      state.webPath = data
    },
    SET_WEB_TITLE: (state, data) => {
      state.webTitle = data
    },
    SET_WEB_TYPES: (state, data) => {
      state.webTypes = data
    },
  },
  actions: {
  }
}
