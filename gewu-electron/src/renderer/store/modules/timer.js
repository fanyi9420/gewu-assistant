export default {
  state: {
    //休息定时器
    timerRest: {
      //休息时间
      'restTime': 20,
      //休息开关
      'restSwitch': true,
    },
    //定时任务
    timerClock: [],
    //定时任务列表
    timerTask: [],
    isShowTask: false
  },
  mutations: {
    SET_TIMER_REST: (state, data) => {
      state.timerRest = data
    },
    SET_TIMER_CLOCK: (state, data) => {
      state.timerClock = data
    },
    SET_TIMER_TASK: (state, data) => {
      state.timerTask = data
    },
    SET_TIMER_TASK_SHOW: (state, data) => {
      state.isShowTask = data
    },
  },
  actions: {
    setTimerRest({ commit, state }, data) {
      commit('SET_TIMER_REST', Object.assign({}, data));
    }
  }
}
