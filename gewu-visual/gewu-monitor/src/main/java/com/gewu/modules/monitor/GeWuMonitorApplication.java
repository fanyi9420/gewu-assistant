package com.gewu.modules.monitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import de.codecentric.boot.admin.server.config.EnableAdminServer;

/**
 * 监控中心
 * 
 * @author gewu
 */
@EnableAdminServer
@SpringBootApplication
public class GeWuMonitorApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(GeWuMonitorApplication.class, args);
    }
}
