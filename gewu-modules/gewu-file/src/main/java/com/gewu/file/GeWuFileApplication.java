package com.gewu.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import com.gewu.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 文件服务
 *
 * @author gewu
 */
@EnableCustomSwagger2
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class GeWuFileApplication {
    public static void main(String[] args) {
        SpringApplication.run(GeWuFileApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  文件服务模块启动成功");
    }
}
