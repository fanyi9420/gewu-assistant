package com.gewu.gen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.gewu.common.security.annotation.EnableCustomConfig;
import com.gewu.common.security.annotation.EnableRyFeignClients;
import com.gewu.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 代码生成
 * 
 * @author gewu
 */
@EnableCustomConfig
@EnableCustomSwagger2   
@EnableRyFeignClients
@SpringBootApplication
public class GeWuGenApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(GeWuGenApplication.class, args);
    }
}
