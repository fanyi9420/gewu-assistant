package com.gewu.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.gewu.common.security.annotation.EnableCustomConfig;
import com.gewu.common.security.annotation.EnableRyFeignClients;
import com.gewu.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 定时任务
 *
 * @author gewu
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class GeWuJobApplication {
    public static void main(String[] args) {
        SpringApplication.run(GeWuJobApplication.class, args);
    }
}
