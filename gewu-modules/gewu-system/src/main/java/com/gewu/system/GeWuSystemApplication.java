package com.gewu.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.gewu.common.security.annotation.EnableCustomConfig;
import com.gewu.common.security.annotation.EnableRyFeignClients;
import com.gewu.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 系统模块
 * 
 * @author gewu
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class GeWuSystemApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(GeWuSystemApplication.class, args);
    }
}
